<?php
namespace MediaWiki\Extension\WikiDesigner;
class SpecialWikiDesigner extends \SpecialPage {
	function __construct() {
		parent::__construct( 'WikiDesigner', 'editinterface' );
	}

	function execute( $par ) {
		$request = $this->getRequest();
		$output = $this->getOutput();
		$this->setHeaders();

		# Get request data from, e.g.
		$param = $request->getText( 'param' );
		$output->addModules( [ 'ext.wikiDesigner' ] );

		$currentCSS = explode("\n", wfMessage('wikiDesigner.css')->plain());
		$parsedCSS = [];

		for ($i = 0; $i < sizeof($currentCSS); $i++) {
			$line = trim($currentCSS[$i], " \n\r\t\v\x00;");
			if(str_starts_with($line, "--")){
				$line = explode(":", $line);
				$parsedCSS[$line[0]] = trim($line[1]);
			}
		}

		function getValue($key, $array){
			if(array_key_exists($key, $array)){
				$newKey = $array[$key];
				if(str_starts_with($newKey, 'var(--')){
					$newKey = getValue( trim(str_replace('var(', '', $newKey), "  \n\r\t\v\x00;)"), $array);
				}
				return $newKey;
			}
			else {
				return '';
			}
		}

		function makeColorOption($name, $label, $array, $auto=false){
			return '<div class="wikiDesigner__option' . (($auto) ? ' auto' : '') . '">
			<label>'. $label . '</label>
			<input name="' . $name . '" type="color" value="' . getValue($name, $array) . '" onchange="mw.WikiDesigner.updateStylesheet(this.name, this.value)"' . (($auto) ? ' disabled' : '') . '></input>
			</div>';
		}

		$output->addHTML(
		'<noscript>' . wfMessage('wikidesigner-noscript-warning') . '</noscript>
		
		<div class="wikiDesigner__container">
			<style id="wikiDesigner__preview">' . wfMessage('wikiDesigner.css')->plain() . '</style>
			<form class="wikiDesigner">
				<div class="wikiDesigner__options-group">
					<legend>Body</legend>
					' . makeColorOption("--wiki-body-background-color", "Body background color", $parsedCSS) . '
					' . makeColorOption("--wiki-body-background-color--secondary", "Body background color secondary", $parsedCSS, true) . '
				</div>

				<div class="wikiDesigner__options-group">
					<legend>Content</legend>
					' . makeColorOption("--wiki-content-background-color", "Content background color", $parsedCSS) . '
					' . makeColorOption("--wiki-content-background-color--secondary", "Content background color secondary", $parsedCSS, true) . '
					<div class="wikiDesigner__option"><label>Opacity</label><input name="--wiki-content-background-opacity" type="range" min="0" max="1" step="0.05" value="' . getValue('--wiki-content-background-opacity', $parsedCSS) . '" onchange="mw.WikiDesigner.updateContentOpacity(this.value)"></input></div>
					' . makeColorOption("--wiki-content-border-color", "Border color", $parsedCSS) . '
					' . makeColorOption("--wiki-content-text-color", "Border color", $parsedCSS, true) . '
					' . makeColorOption("--wiki-content-link-color", "Link color", $parsedCSS) . '
					' . makeColorOption("--wiki-content-link-color--visited", "Visited link color", $parsedCSS) . '
					' . makeColorOption("--wiki-content-redlink-color", "Redlink color", $parsedCSS) . '
				</div>

				<div class="wikiDesigner__options-group">
					<legend>Headings</legend>
					' . makeColorOption("--wiki-heading-color", "Heading color", $parsedCSS) . '
					<div class="wikiDesigner__option"><label>Font family</label><select name="heading-font-family">
						<option value="serif" style="font-family:serif;">Serif</option>
						<option value="sans-serif" style="font-family:sans-serif;">Sans-serif</option>
						<option value="verdana" style="font-family:verdana;">Verdana</option>
					</select></div>
				</div>

				<div class="wikiDesigner__options-group">
					<legend>Accent</legend>
					' . makeColorOption("--wiki-accent-color", "Accent color", $parsedCSS) . '
				</div>

				<div class="wikiDesigner__options-group">
					<legend>Sidebar</legend>
					' . makeColorOption("--wiki-sidebar-background-color", "Sidebar background color", $parsedCSS) . '
					<div class="wikiDesigner__option"><label>Opacity</label><input name="--wiki-sidebar-background-opacity" type="range" min="0" max="1" step="0.05" value="' . getValue('--wiki-sidebar-background-opacity', $parsedCSS) . '" onchange="mw.WikiDesigner.updateSidebarOpacity(this.value)"></input></div>
					' . makeColorOption("--wiki-sidebar-border-color", "Sidebar border color", $parsedCSS) . '
					' . makeColorOption("--wiki-sidebar-link-color", "Sidebar-link color", $parsedCSS) . '
					' . makeColorOption("--wiki-sidebar-heading-color", "Sidebar heading color", $parsedCSS) . '
				</div>

				<div class="wikiDesigner__options-group">
					<legend>Navigation</legend>
					' . makeColorOption("--wiki-navigation-background-color", "Nav background color", $parsedCSS) . '
					' . makeColorOption("--wiki-navigation-background-color--secondary", "Nav background color secondary", $parsedCSS, true) . '
					' . makeColorOption("--wiki-navigation-selected-background-color", "Selected background color", $parsedCSS) . '
					' . makeColorOption("--wiki-navigation-border-color", "Nav border color", $parsedCSS) . '
					' . makeColorOption("--wiki-navigation-selected-border-color", "Selected border color", $parsedCSS) . '
					' . makeColorOption("--wiki-navigation-text-color", "Nav text color", $parsedCSS) . '
					' . makeColorOption("--wiki-navigation-selected-text-color", "Selected text color", $parsedCSS) . '
				</div>

			</form>
			<input type="button" onclick="mw.WikiDesigner.save()" value="Save"/>
		</div>'
		);

		$output->addWikitextAsInterface('
			{| class="wikitable"
			|+ An example wikitable
			|-
			! Key !! Value !! Other
			|-
			| Jeff || Guy || Cool
			|-
			| You || Person || Likes [[{{FULLPAGENAME}}]] and [[Main Page]]
			|-
			| Somebody else || I don\'t know || According to all known laws of aviation, there is no way [[this link]] should work
			|}
		');
	}
}